" Open netrw in vertical split
nnoremap <leader>v :Vex<CR><CR>

" Open netrw in horizontal split
nnoremap <leader>h :Hex<CR><CR>

" Change dir view
let g:netrw_liststyle = 3

" Remove banner
let g:netrw_banner = 0

" Set dir explorer width
let g:netrw_winsize = 25

let g:netrw_browse_split = 2
let g:netrw_banner = 0
let g:netrw_winsize = 25
