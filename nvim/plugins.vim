" Plugins:

call plug#begin('~/AppData/nvim/bundle')
Plug 'junegunn/goyo.vim'
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'godlygeek/tabular' | Plug 'plasticboy/vim-markdown'
Plug 'iamcco/markdown-preview.nvim', { 'do': 'cd app & yarn install' }
Plug 'pangloss/vim-javascript'
Plug 'mxw/vim-jsx'
Plug 'ianks/vim-tsx'
Plug 'leafgarland/typescript-vim'
Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
Plug 'junegunn/fzf.vim'
Plug 'airblade/vim-rooter'
Plug 'voldikss/vim-floaterm'
Plug 'liuchengxu/vim-which-key'
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'
Plug 'gruvbox-community/gruvbox'
Plug 'lambdalisue/fern.vim'
Plug 'tpope/vim-surround'
Plug 'mhinz/vim-signify'
Plug 'tpope/vim-fugitive'
Plug 'tpope/vim-rhubarb'
Plug 'junegunn/gv.vim'
Plug 'kevinhwang91/rnvimr'
call plug#end()


" Plugin Configurations
source $HOME/AppData/Local/nvim/plug-config/fzf.vim
source $HOME/AppData/Local/nvim/plug-config/floaterm.vim
source $HOME/AppData/Local/nvim/plug-config/which-key.vim
source $HOME/AppData/Local/nvim/plug-config/goyo.vim
source $HOME/AppData/Local/nvim/plug-config/fern.vim
source $HOME/AppData/Local/nvim/plug-config/signify.vim
source $HOME/AppData/Local/nvim/plug-config/rnvimr.vim
source $HOME/AppData/Local/nvim/themes/airline.vim
source $HOME/AppData/Local/nvim/themes/gruvbox.vim
